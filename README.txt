This project contains the basic ideas of creating web APIS. inside there are 3 apps with some functionality app TechInterns contains all information about interns and displays it. it also shows the iplementation of post, put, and get. app test_api2 tells how filter() works and what output we are getting on using filter. similarly test_api tests the functinality for exclude.

To run the Project six_month_intern
	1. Go to the directory respective directory "cd six_month_intern"
	2.  To run Api
		2.1. make migrations if required by python "manage.py makemigrations <app-name>".
		2.2. type "python manage.py migrate <app-name>".
		2.3. run server by "python manage.py runserver".
		2.4. go to the webpage and type the repective url.
			url for TechInterns
				"http://127.0.0.1:8080/home/techinterns/interninfoview/"
			url for test_api
				"http://127.0.0.1:8080/home/testapi/PersonView/"
			url for test_api2
				"http://127.0.0.1:8080/home/testapi2/PersonView/"

for creating second migration file in a particular api just make changes in table from models
like for example: add a new coloum to the table. and than do another migration for that api.

