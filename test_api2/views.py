# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.views.generic import View
# Create your views here.
from django.http import HttpResponse, QueryDict
import json


from test_api2.models import Information
#Def index(request):
#	return HttpResponse("hello , welcome to happay.")

class PersonView(View):

	def get(self, request, *args, **kwargs):
		#import pdb; pdb.set_trace()
		_params = request.GET
		data = []
		try:
			#i = Information.objects.get(id=_params.get('id'))
			#data['error'] = "getting an error"
		#first_name = _params.get('id')
			persons = Information.objects.filter(id=_params.get('id'))
			#print len(persons)
			#print persons
			for i in persons:
				data.append({
					'first_name': i.first_name,
					'last_name': i.last_name,
					'user_name': i.user_name,
				})
			return HttpResponse(json.dumps(data), content_type='application/json')
		except Information.DoesNotExist as e:
			print "e not in strings"
		