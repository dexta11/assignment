# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class TestApi2Config(AppConfig):
    name = 'test_api2'
