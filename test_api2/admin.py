# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models import Information

admin.site.register(Information)
# Register your models here.
