# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Information(models.Model):
	first_name=models.CharField(max_length=255)
	last_name=models.CharField(max_length=255,null=True,blank=True)
	user_name=models.CharField(max_length=255,unique=True)
	user_id = models.CharField(max_length=255, null = True)
	def __unicode__(self):
		return str(self.first_name)+ ' ' + str(self.last_name)