# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import url
from django.test import TestCase
from django.views.decorators.csrf import csrf_exempt
from test_api.views import PersonView

# Create your tests here.
urlpatterns = [
   url(r'^testapi/PersonView/$',csrf_exempt(PersonView.as_view())),
]