# -*- coding: utf-8 -*-
# models are used to tell how data is managed in database
from __future__ import unicode_literals 

""" importing  explicitly marking up all
unicode string literals with u'' prefixes would help to
avoid unintentionally changing the existing Python 2 API. """

from django.db import models
from managers import CheckLength
"""
	Each model is a Python class that subclasses django.db.models.Model.
"""
# Create your models here.
"""
	class to create a table interninfo which contains all 
	the information about the interns.
"""
class InternInfo(models.Model): 
	intern_name = models.CharField(max_length = 255, null = True)
	intern_last_name = models.CharField(max_length = 255, null = True)
	intern_college_id = models.CharField(max_length = 255, null = False, unique = True)
	#intern_employe_id = models.CharField(max_length = 255, null = True, unique = True)
	intern_university = models.CharField(max_length = 255, null = True)
	join_date = models.DateField()
	objects = CheckLength()
#class Meta:
	#unique_together(("intern_college_id", "intern_employee_id"),)

def __unicode__(self):
		return str(self.intern_name)+ ' ' + str(self.intern_last_name)

#class Meta:
	#ordering = ('information_stored',)
