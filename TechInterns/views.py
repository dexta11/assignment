# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.views.generic import View
# Create your views here.
from django.http import HttpResponse, QueryDict, HttpResponseNotFound

import json
from TechInterns.models import InternInfo
#   return HttpResponse("hello , welcome to happay.")

class InternInfoView(View):


	""" GET() is used to fetch the data from the table """
	def get(self, request, *args, **kwargs):
		#import pdb; pdb.set_trace()
		_params = request.GET# get a string which can also be none, 0 , or null
		data = []# creating a list of data
		interns = InternInfo.objects.all() # retrieving all the fields of table
		if interns.count() > 0:
			for intern_detail in interns:#iterating through out each entry in the table
				data.append({
				'intern_name': intern_detail.intern_name,
				'intern_last_name': intern_detail.intern_last_name,
				'intern_university': intern_detail.intern_university,
				'intern_college_id':intern_detail.intern_college_id,
				#'intern_employee_id':intern_detail.intern_employee_id,
				'join_date':intern_detail.join_date.isoformat(),
				})
			return HttpResponse(json.dumps(data), content_type='application/json')
		else:
			return HttpResponseNotFound("No entries in table no object is created")

	""" post is used to add data """
	def post(self, request, *args, **kwargs):
	
		_arg1 = QueryDict(request.body)
		#_arg1 = QueryDict(_arg1)#json.loads(_arg1)# converts _arg1 to json format
		data = _arg1
		first_n = _arg1.get('intern_name')
		last_n = _arg1.get('intern_last_name')
		user_n = _arg1.get('intern_university')
		user_i = _arg1.get('intern_college_id')
		#user_e = _arg1.get('intern_employee_id')
		date_i = _arg1.get('join_date')
		try:
			per = InternInfo(intern_name = first_n, intern_last_name = last_n, intern_university = user_n,intern_college_id = user_i, join_date= date_i)
			check = InternInfo.objects.is_valid(data.get('intern_name'),data.get('intern_last_name'))
			if check:
				per.save()
				return HttpResponse("data of name added successfully")
			#return HttpResponse('{"message":"Added successfully"}', content_type='application/json')   
			else:
				return HttpResponseNotFound("length of first_name and last_name is exceeded")
			#if(per.is_valid()):
				#per.save()
			
			#return HttpResponse("unique values are asked for id's")
			
		except: return HttpResponse("unique values are asked for id's")
		#except:
		#return HttpResponseNotFound("unique constraint Violated")
		
	""" PUT() is used to update the entries in table """
	def put(self,request,*args,**kwargs):
		#data=request.PUT
		#q=request.GET
		#import pdb; pdb.set_trace()
		_arg1 = request.body
		data = QueryDict(_arg1)
		#print request.body
		#print data
		#data = data.get('data')

		#request.body
		#print data.get('user_id')
		try:
			p = InternInfo.objects.get(intern_college_id=data.get('intern_college_id'))
			p.intern_college_id=data.get('intern_college_id')
		#p.intern_employee_id = data.get('intern_employee_id')
			p.first_name=data.get('intern_name')
			p.last_name=data.get('intern_last_name')
			p.intern_university = data.get('intern_university')
			p.join_date = data.get('join_date')
			p.save()
			return HttpResponse('{"message":"Edited successfully"}', content_type='application/json')   
		except:
			return HttpResponseNotFound("No data is present with given id")