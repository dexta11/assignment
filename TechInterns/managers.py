from django.db.models import Manager
class CheckLength(Manager):
	def is_valid(self, intern_name, intern_last_name):
		if len(intern_name) <= 255 and len(intern_last_name) <= 255:
			return True
		else:
			return False
