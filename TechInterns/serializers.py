"""from snippets.models import TechInterns
from snippets.serializers import TechInternsSerializers
from rest_framework.renders import JSONRenderer
from rest_framework.parsers import JSONParser

class TechInternsSerializers(serializers.ModelsSerializer):
	class Meta:
			model = TechInterns
			fields = ("intern_id","intern_name", "intern_university", "join_date")"""