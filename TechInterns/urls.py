from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from TechInterns.views import InternInfoView

urlpatterns = [
   url(r'^techinterns/interninfoview/$',csrf_exempt(InternInfoView.as_view())),
]